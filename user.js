// User Controller
const User = require('../models/user')

module.exports.checkEmail = (body) => {
    // The mongoDB find() method always returns and array
    return User.find({email: body.email}).then(result => {
        if(result.length > 0){ // if we found an email
            return true;
        }else{ // otherwise return false empty array no email found
            return false;
        }
    })
}

module.exports.register = (body) => {
    console.log(body)
    let newUser = new User({
        firstName: body.firstName,
        lastName: body.lastName,
        email: body.email,
        password: body.password,
        mobileNo: body.mobileNo
    })

    return newUser.save().then((user, error) => {
        if(error){ // if there's and error
            return false;
        }else{
            return true;
        }
    })   
}

